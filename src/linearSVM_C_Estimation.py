# -*- coding: utf-8 -*-
"""
This file contains some 
"""
from mllib.pipeline import Pipeline
from mllib.Classifier import SVM_linear
from mllib.load_data import Data
from mllib.validation import K_fold_cross_validation
from mllib.preprocessing import PCA, Z_Norm
from mllib.utils import shuffle

import numpy as np
import time
import matplotlib.pyplot as plt


if __name__ == "__main__":
    lambdas = np.logspace(-3, 2, 30)
    p = []
    
    for C in lambdas:
        p.append(Pipeline([Z_Norm(), SVM_linear(1, C)]))
    
    (dataset, label) = Data("PULSAR").run()
    (dataset, label) = shuffle(dataset, label, 140)
    start = time.time();
    print("Training all the models with 3-fold.")

    k = K_fold_cross_validation(3, p)

    k.validate(dataset, label)


    print(f"Training finished in {time.time() - start} s")
    
    # I need to plot for different values of lamba ad different prior, the minDCF
    # on the x-axis I have lambda, on y the min_DCF, and different color for the different prior
    start = time.time()
    min1 = k.mindcf(.5)
    min2 = k.mindcf(.9)
    min3 = k.mindcf(.1)
    print(f"minDCF computed in {time.time() - start} s")
    
    plt.figure()
    plt.plot(lambdas, min1, color="r", label="prior=0.5")
    plt.plot(lambdas, min2, color="b", label="prior=0.9")
    plt.plot(lambdas, min3, color="g", label="prior=0.1")
    plt.xlim([min(lambdas), max(lambdas)])
    plt.xscale("log", base=10)
    plt.legend()
    plt.xlabel("C")
    plt.savefig("img/bayes/SVM_major")

    
    