# -*- coding: utf-8 -*-
"""
Created on Thu Apr 21 10:44:23 2022

@author: gabbi
"""

from mllib.pipeline import Pipeline
from mllib.Classifier import mvg_classifier, naive_bayes_classifier, tied_gaussian_classifier, linear_logistic_regression, quadratic_logistic_regression, SVM_linear, SVM_polynomial, SVM_RBF
from mllib.load_data import Data
from mllib.validation import K_fold_cross_validation
from mllib.preprocessing import PCA, Z_Norm
from mllib.utils import shuffle
import numpy as np
from mllib.utils import split_db_2to1, shuffle
import time

if __name__ == "__main__":
    p = [
        # Pipeline([PCA(8, 4), mvg_classifier()]),
        # Pipeline([Z_Norm(), PCA(8, 6), mvg_classifier(.5)]),
        # Pipeline([Z_Norm(), PCA(8, 5), mvg_classifier(.5)]),
        # Pipeline([Z_Norm(), mvg_classifier(.5)]),
        # Pipeline([PCA(8,7), tied_gaussian_classifier(.5)]),
        # Pipeline([Z_Norm(), tied_gaussian_classifier()]),
        # Pipeline([Z_Norm(), tied_gaussian_classifier(.5)]),
        # Pipeline([Z_Norm(), linear_logistic_regression(1e-3, .5)]),
        # Pipeline([linear_logistic_regression(1e-3, .9)]),
        # Pipeline([linear_logistic_regression(1e-3, .1)]),
        # Pipeline([Z_Norm(), linear_logistic_regression(1e-4, .5)]),
        # Pipeline([Z_Norm(), linear_logistic_regression(1e-4, .5)]),
        # Pipeline([Z_Norm(), linear_logistic_regression(1e-4, .9)]),
        # Pipeline([Z_Norm(), linear_logistic_regression(1e-4, .1)]),
        # Pipeline([Z_Norm(), linear_logistic_regression(1e-6, .5)]),
        # Pipeline([Z_Norm(), linear_logistic_regression(1e-6, .9)]),
        # Pipeline([Z_Norm(), linear_logistic_regression(1e-6, .1)]),
        # Pipeline([Z_Norm(), linear_logistic_regression(1e-1, .5)]),
        # Pipeline([Z_Norm(), linear_logistic_regression(1e-1, .9)]),
        # Pipeline([Z_Norm(), linear_logistic_regression(1e-1, .1)]),
        # Pipeline([quadratic_logistic_regression(1e-6)]),
        # Pipeline([quadratic_logistic_regression(1e-3)]),
        Pipeline([Z_Norm(), SVM_linear(1, 1e-2)]),
        Pipeline([Z_Norm(), SVM_linear(1, 1e-3)]),
        # Pipeline([linear_logistic_regression(1e-4, .5)]),
        # Pipeline([linear_logistic_regression(1e-6, .9)]),
        # Pipeline([linear_logistic_regression(1e-6, .1)]),
        # Pipeline([linear_logistic_regression(1e-1, .5)]),
        # Pipeline([linear_logistic_regression(1e-1, .9)]),
        # Pipeline([linear_logistic_regression(1e-1, .1)]),
        # Pipeline([Z_Norm(), quadratic_logistic_regression(1e-6)]),
        # Pipeline([SVM_linear(1, .1)]),
        # Pipeline([SVM_polynomial(0, 1, 2, 0)]),
        # Pipeline([SVM_polynomial(1, 1, 2, 0)]),
        # Pipeline([SVM_polynomial(0, 1, 2, 1)]),
        # Pipeline([SVM_polynomial(1, 1, 2, 1)]),
        # Pipeline([SVM_RBF(0, 1, 1)]),
        # Pipeline([SVM_RBF(0, 1, 10)]),
        # Pipeline([SVM_RBF(1, 1, 1)]),
        # Pipeline([SVM_RBF(1, 1, 10)])
    ]
    
    (dataset, label) = Data("PULSAR").run()
    (dataset, label) = shuffle(dataset, label, 140)
    (DTR, LTR), (DTE, LTE) = split_db_2to1(dataset, label)

    k = K_fold_cross_validation(3, p)
    start = time.time();
    k.validate(dataset, label)
    print(f"Training finished in {time.time() - start} s")    
    k.print_result()
    # k.accuracy()
    print(f"min DCF computed in {time.time() - start} s")
    start = time.time();
    k.BayesErrorPlot(calibrated=True)
    print(f"Bayes error plot in {time.time() - start} s")
    # print("\n\nEvaluation set:\n\n")
    
    # for model in p:
    #     model.train((DTR, LTR))
    #     (score, _) = model.run((DTE, LTE))
    
    #     prediction = score > 0
    #     acc = np.array([prediction == LTE]).sum()/LTE.shape[0]
    #     print(f"{model} \t Error rate: {(1 - acc)*100:6.3f} %")