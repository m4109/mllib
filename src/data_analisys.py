import numpy as np
import matplotlib.pyplot as plt
import os
import seaborn as sns
from mllib.load_data import Data

sns.set_theme(style="white")

def mcol(a):
    return a.reshape(a.size, 1)

def mrow(a):
    return a.reshape(1, a.size)

def load_pulsar(filename):
    list_of_rows = []
    labels = []
    with open(filename, "r") as file:
        for line in file:
            row = line.split(",")
            attr = [float(e) for e in row[0:8]]
            label = int(row[8].rstrip())
            labels.append(label)
            list_of_rows.append(mcol(np.array(attr)))
    return np.hstack(list_of_rows), np.array(labels)

def get_version():
    v = 0
    with open("version.txt", "r") as f:
        for line in f:
            v = int(line.rstrip())
    with open("version.txt", "w") as f:
        f.write(f'{v+1}')
    return v
        
attribute_names = {
    0: "Mean of the integrated profile",
    1: "Standard deviation of the integrated profile",
    2: "Excess kurtosis of the integrated profile",
    3: "Skewness of the integrated profile",
    4: "Mean of the DM-SNR curve",
    5: "Standard deviation of the DM-SNR curve",
    6: "Excess kurtosis of the DM-SNR curve",
    7: "Skewness of the DM-SNR curve"
}


def draw_hist(d, l):
    d0 = d[:, l == 0]
    d1 = d[:, l == 1]
    folder = f'img/feature/'
    for i in range(8):
        # for each attribute, print and histogram
        # every attribute is a row
        plt.figure()
        plt.xlabel(attribute_names[i])
        plt.hist(d0[i, :], bins=20, alpha= 0.8, label="Non pulsar", density=True)
        plt.hist(d1[i, :], bins=20, alpha= 0.8, label="Pulsar", density=True)
        plt.legend()
        plt.tight_layout()
        path = f'{folder}/{attribute_names[i].replace(" ", "_")}'
        print(path)
        plt.savefig(path)
    plt.show()
    
def compute_empirical_mean(X: np.ndarray) -> np.ndarray:
      """Compute the empirical mean for a dataset. 
      
      It returns a column vector, each element corresponds to the mean of the given row(representing an
      attribute).

      #### Parameters:
       - X: dataset in column form(each column represent a sample)
      """
      return mcol(X.mean(1))

def compute_wc_covariance_matrix(d, l):
     Sw = 0;
     for i in [0, 1, 2]:
         Sw += (l==i).sum()*compute_empirical_cov(d[:, l == i])
     return Sw/d.shape[1]


def compute_empirical_cov(X: np.ndarray) -> np.ndarray:
   """
   Compute the empirical covariance matrix for a dataset. 
   #### Parameters:
    - X: dataset in column form(each column represent a sample)
   """
   mu =  compute_empirical_mean(X)
   cov = np.dot((X - mu), (X - mu).T) / X.shape[1]
   return cov  

def draw_heatmap(c, title) -> None:
    plt.figure(figsize=(6,6))
    cmaps = {
        "Dataset": "Greys",
        "Class True": "Reds",
        "Class false": "Blues"
    }
    # mask = np.tri(c.shape[0], c.shape[0], -1).T
    sns.heatmap(c, cmap=cmaps[title], square=True, linewidths=.5, cbar=False)
    plt.savefig(f"img/heatmaps/{title}")

def pearson_coefficient(d: np.ndarray) -> np.ndarray:
    c = compute_empirical_cov(d)
    n = c.shape[0]
    p = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            p[i, j] = np.abs(c[i, j]/(np.sqrt(c[i, i])*np.sqrt(c[j, j])))
    return p

def z_normalization(d: np.ndarray) -> np.ndarray:
    res = d
    mu = mcol(compute_empirical_mean(d))
    standard_dev = mcol(d.std(1))
    res = (d - mu)/standard_dev
    
    return res

if __name__ == "__main__":
    d, l = Data("PULSAR").run()
    z = z_normalization(d)
    draw_hist(z, l)
    # Plotting the covariance matrix as a heatmap
    z1 = z[:, l == 0]
    z2 = z[:, l == 1]
    
    c = pearson_coefficient(z)
    draw_heatmap(c, "Dataset")
    c1 = pearson_coefficient(z1)
    draw_heatmap(c, "Class True")
    c2 = pearson_coefficient(z2)
    draw_heatmap(c, "Class false")
    
    print(f"Positive class: {l.sum()}\nNegative Class: {l.shape[0] - l.sum()}\n\n")
    print("Dynamics of the attributes(Before z-normalization)\n")
    for (i, name) in attribute_names.items():
        print(f"{name}: [{d[i,].min()}, {d[i, ].max()}] ")
    
    print("\n\nDynamics of the attributes(After z-normalization)\n")
    for (i, name) in attribute_names.items():
        print(f"{name}: [{z[i,].min()}, {z[i, ].max()}] ")

