# -*- coding: utf-8 -*-
"""
Created on Thu Apr 21 11:21:48 2022

@author: gabbi
"""
from mllib.step import Pipe
import numpy as np
from mllib.probability import *
import matplotlib.pyplot as plt

class Split(Pipe):
    def __init__(self, ratio):
        self.ratio = ratio
        
    def split_db_2to1(self, D, L, seed=0):
        """
        Parameters
        ----------
        D : np.ndarray 
            Dataset in column form(each column represent a sample).
        L : np.ndarray
            array of labels.
        seed : int, optional
            seed for the permutation. The default is 0.

        Returns
        -------
        ((np.ndarray, np.ndarray),(np.ndarray, np.ndarray))
            The two partition of the dataset and the label.
        """
        nTrain = int(D.shape[1]*self.ratio)
        np.random.seed(seed)
        idx = np.random.permutation(D.shape[1])
        idxTrain = idx[0:nTrain]
        idxTest = idx[nTrain:]
        DTR = D[:, idxTrain]
        DTE = D[:, idxTest]
        LTR = L[idxTrain]
        LTE = L[idxTest]
        return (DTR, LTR), (DTE, LTE)

class PCA(Pipe):
    def __init__(self, n, m):
        self.n = n
        self.m = m
        self.name = f'PCA({self.n} to {self.m} feature space)'
    
    def train(self, args):
        (dataset, l) = args
        C = compute_empirical_cov(dataset)
        s, U = np.linalg.eigh(C)
        self.P = U[:, ::-1][:, 0:self.m]
        y = np.dot(self.P.T, dataset)
        if (self.m == 2):
            plt.figure()
            d1 = y[:, l == 0]
            d2 = y[:, l == 1]
            plt.scatter(d1[0, :], d1[1, :], color="r") 
            plt.scatter(d2[0, :], d2[1, :], color="b")  
            plt.show()
        if (self.m == 1):
                plt.figure()
                print(y.shape, l.shape)
                d1 = np.ravel(y)[l == 0]
                d2 = np.ravel(y)[l == 1]
                plt.hist(d1,  bins=60, density=True) 
                plt.hist(d2, bins=60, density=True)  
                plt.show()
        return (y, l)

    def run(self, args):
        (dataset, _) = args
        # C = compute_empirical_cov(dataset)
        # s, U = np.linalg.eigh(C)
        # P = U[:, ::-1][:, 0:self.m]
        y = np.dot(self.P.T, dataset)
        return (y, _)
    
class Z_Norm(Pipe):
    def __init__(self):
        self.mu = None
        self.std = None
        self.name = f"Z, "

    
    def train(self, args):
        (dataset, _) = args
        self.mu, self.std = z_normalization(dataset)
        # print(f"Estimated parameter: {self.mu}, {self.std}")
        return ((dataset - self.mu)/self.std, _)
    
    def run(self, args):
        (dataset, _) = args
        # print(f"Parameter using while running: {self.mu}, {self.std}")
        return ((dataset - self.mu)/self.std, _)
        
        