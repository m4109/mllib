class Pipeline():
    def __init__(self):
        self.steps = []
        self.trained = False
    
    def __init__(self, steps):
        self.steps = steps
        self.trained = False
        
    def add_step(self, step):
        self.steps.append(step)
    
    def __str__(self):
        res = ""
        for step in self.steps:
            res += step.__str__()
        return res
    
    def train(self, args):
        temp = args
        for step in self.steps:
            temp = step.train(temp)
        self.trained = True
        return temp
    
    def run(self, args):
        if self.trained == False:
            print("Model not trained yet!")
            return
        temp = args
        for step in self.steps:
            temp = step.run(temp)
        return temp
            