# -*- coding: utf-8 -*-
"""
Created on Thu Apr 21 10:35:36 2022

@author: gabbi
"""
import sklearn.datasets
import numpy as np
from mllib.utils import mcol
import os

def load_iris():
    D, L = sklearn.datasets.load_iris()['data'].T, sklearn.datasets.load_iris()['target']
    return D, L

def load_iris_binary():
    D, L = sklearn.datasets.load_iris()['data'].T, sklearn.datasets.load_iris()['target']
    D = D[:, L != 0] # We remove setosa from D
    L = L[L!=0] # We remove setosa from L
    L[L==2] = 0 # We assign label 0 to virginica (was label 2)
    return D, L

def load_MNIST():
    D, L = sklearn.datasets.load_digits()['data'].T, sklearn.datasets.load_digits()['target']
    return D, L

def load_pulsar(filename):
    list_of_rows = []
    labels = []
    print(os.getcwd())
    with open(filename, "r") as file:
        for line in file:
            row = line.split(",")
            attr = [float(e) for e in row[0:8]]
            label = int(row[8].rstrip())
            labels.append(label)
            list_of_rows.append(mcol(np.array(attr)))
    return np.hstack(list_of_rows), np.array(labels)

class Data():
    def __init__(self, filename):
        self.name = filename
        
    def run(self):
        if self.name == "IRIS":
            return load_iris()
        elif self.name == "MNIST":
            return load_MNIST()
        elif self.name == "PULSAR":
            return load_pulsar("mllib/datasets/pulsarTrain.txt")
        elif self.name == "IRIS_BINARY":
            return load_iris_binary()