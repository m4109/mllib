# -*- coding: utf-8 -*-
"""
Created on Thu Apr 21 10:19:44 2022

@author: gabbi
"""
from mllib.step import Sink
from mllib.utils import mcol, mrow
from mllib.probability import *

import numpy as np
import scipy.special

psi = 0.1

def ml_gaussian_fit(dataset: np.ndarray) -> (np.ndarray, np.ndarray):
    """
        Compute the maximum likelihood parameter for a multivariate gaussian distribution
        
        #### Parameters:
        - dataset: dataset in column form(each column represent a sample)
    """
    mu = compute_empirical_mean(dataset)
    C = compute_empirical_cov(dataset)
    return (mu, C)

class mvg_classifier(Sink):
    
    def __init__(self, prior):
       self.name = f"Multivariate Gaussian Classifier ($\pi_T = {prior})"
       self.trained = False
       self.prior = prior
       
       # Model parameter
       self.ml_parameter = None
       self.prior_class_probabilities = None
    
    def train(self, args):
        (training_set, training_label) = args

        ml_parameter = {}
        self.distinct_training_label = training_label.max() + 1
        self.prior_class_probabilities = np.array([self.prior, 1 - self.prior])
        
        for label in range(self.distinct_training_label):
            ml_parameter[label] = ml_gaussian_fit(training_set[:, training_label == label])
            
        self.ml_parameter = ml_parameter
        self.trained = True
        return (training_set, training_label)
        
        
    def run(self, args):
        (dataset, labels) = args
        #S = np.zeros((self.distinct_training_label, dataset.shape[1]))
        logS = np.zeros((self.distinct_training_label, dataset.shape[1]));
        for label in range(self.distinct_training_label):
            # extract the parameters for the corresponding label.
            (mu, cov) = self.ml_parameter[label]
            # Each row of the score matrix represent the product of(P(x|C)*P(C)) for every sample.
            # The parameter are the same on each row.
            #S[label, :] = mrow(pdf_gaussian(dataset, mu, cov)) * self.prior_class_probabilities[0, label]
            logS[label, :] = mrow(logpdf_GAU_ND(dataset, mu, cov))  + np.log(self.prior_class_probabilities[label])

        logSMarginal = mrow(scipy.special.logsumexp(logS, axis=0))
        #SMarginal = mrow(S.sum(0))

        #SPost = S/SMarginal
        logSPost = logS - logSMarginal
        
        llr = logSPost[1, :] - logSPost[0, :]

        # return the index of the maximum value along an axis. axis = 0 means along a column.
        predicted_label = logSPost.argmax(0)
        
        return (np.ravel(llr), labels)

class naive_bayes_classifier(Sink):
    
    def __init__(self, prior):
       self.name = f"Naive Bayes Gaussian Classifier($\pi_T = {prior})"
       self.trained = False
       self.prior = prior
       
       # Model parameter
       self.ml_parameter = None
       self.prior_class_probabilities = None
    
    def train(self, args):
        (training_set, training_label) = args

        ml_parameter = {}
        self.distinct_training_label = training_label.max() + 1;
        self.prior_class_probabilities = np.array([self.prior, 1 - self.prior])
        
        for label in range(self.distinct_training_label):
           (mu, cov) = ml_gaussian_fit(training_set[:, training_label == label])
           ml_parameter[label] = (mu, np.multiply(cov, np.identity(cov.shape[0])))
        self.ml_parameter = ml_parameter
        self.trained = True
        return (training_set, training_label)
        
        
    def run(self, args):
        (dataset, labels) = args
        logS = np.zeros((self.distinct_training_label, dataset.shape[1]))
        for label in range(self.distinct_training_label):
            # extract the parameters for the corresponding label.
            (mu, cov) = self.ml_parameter[label]
            # Each row of the score matrix represent the product of(P(x|C)*P(C)) for every sample.
            # The parameter are the same on each row.
            #S[label, :] = mrow(pdf_gaussian(dataset, mu, cov)) * self.prior_class_probabilities[0, label]
            logS[label, :] = mrow(logpdf_GAU_ND(dataset, mu, cov))  + np.log(self.prior_class_probabilities[label])

        logSMarginal = mrow(scipy.special.logsumexp(logS, axis=0))
        #SMarginal = mrow(S.sum(0))

        #SPost = S/SMarginal
        logSPost = logS - logSMarginal
        llr = logSPost[1, :] - logSPost[0, :]
        # return the index of the maximum value along an axis. axis = 0 means along a column.
        predicted_label = logSPost.argmax(0)
        
        return (np.ravel(llr), labels)

class tied_gaussian_classifier(Sink):
    
    def __init__(self, prior):
       self.name = f"Tied Gaussian Classifier($\pi_T = {prior})"
       self.trained = False
       self.prior = prior
       # Model parameter
       self.ml_parameter = None
       self.prior_class_probabilities = None
    
    def train(self, args):
        (training_set, training_label) = args

        ml_parameter = {}
        self.distinct_training_label = training_label.max() + 1;
        self.prior_class_probabilities = np.array([self.prior, 1 - self.prior])
        
        cov = compute_wc_covariance_matrix(training_set, training_label)
        for label in range(self.distinct_training_label):
            ml_parameter[label] = (compute_empirical_mean(training_set[:, training_label == label]), cov)
            
        self.ml_parameter = ml_parameter
        self.trained = True
        return (training_set, training_label)
        
        
    def run(self, args):
        (dataset, labels) = args
        logS = np.zeros((self.distinct_training_label, dataset.shape[1]))
        for label in range(self.distinct_training_label):
            # extract the parameters for the corresponding label.
            (mu, cov) = self.ml_parameter[label]
            # Each row of the score matrix represent the product of(P(x|C)*P(C)) for every sample.
            # The parameter are the same on each row.
            #S[label, :] = mrow(pdf_gaussian(dataset, mu, cov)) * self.prior_class_probabilities[0, label]
            logS[label, :] = mrow(logpdf_GAU_ND(dataset, mu, cov)) + np.log(self.prior_class_probabilities[label])

        logSMarginal = mrow(scipy.special.logsumexp(logS, axis=0))
        #SMarginal = mrow(S.sum(0))

        #SPost = S/SMarginal
        logSPost = logS - logSMarginal
        
        llr = logSPost[1, :] - logSPost[0, :]
        
        # return the index of the maximum value along an axis. axis = 0 means along a column.
        predicted_label = logSPost.argmax(0)
        
        return (np.ravel(llr), labels)

class linear_logistic_regression(Sink):
    
    def __init__(self, l, prior=.5):
        self.name = f'LLR($\\lambda$ = {l}, $\\pi_T$ = {prior})'
        self.trained = False
        # hyper parameter
        self.l = l
        self.prior = prior
        
        # Model parameter
        self.w = None
        self.b = None
        
        
    def train(self, args):
        # Inner class useful to compute the objective
        class logRegClass:
            def __init__(self, DTR, LTR, l, prior):
                self.DTR = DTR
                self.LTR = LTR
                self.l = l
                self.prior = prior
                
            def logreg_obj(self, v):
                # Compute and return the objective function value. You can
                # retrieve all required information from self.DTR, self.LTR
                Z = self.LTR * 2.0 - 1.0
                M = self.DTR.shape[0]
                w, b = mcol(v[0:M]), v[-1]
                # Regularization term, useful to find solution with 
                # low norm
                reg_term = 0.5*self.l*np.linalg.norm(w)**2
                
                # Score computation
                # 2 classes
                d1 = self.DTR[:, self.LTR == 1]
                d2 = self.DTR[:, self.LTR == 0]
                # print(f"d1: {d1.shape}, d2: {d2.shape}")
                Z1 = Z[Z == 1]
                Z2 = Z[Z == -1]
                # print(f"z1: {Z1.shape}, Z2: {Z2.shape}")
                S1 = w.T @ d1 + b

                cxe1 = np.logaddexp(0, -S1*Z1).mean()
                
                S2 = w.T @ d2 + b
                # print(f"S1: {S1.shape}, S2: {S2.shape}")
                cxe2 = np.logaddexp(0, -S2*Z2).mean()
                # print(f'Reg_term: {reg_term}, Sum: {sum_term}')
                return reg_term + self.prior* cxe1 + (1 - self.prior)*cxe2;
            
        (dataset, label) = args
        logReg = logRegClass(dataset, label, self.l, self.prior)        
        sol =  scipy.optimize.fmin_l_bfgs_b(logReg.logreg_obj, np.zeros((dataset.shape[0] + 1, 1)), approx_grad=True)
        self.w = mcol(sol[0][0:dataset.shape[0]])
        self.b = sol[0][-1]
        # print(f"I'm training the model: w: {self.w}, b: {self.b}")
        self.trained = True
    
    def run(self, args):
        (dataset, labels) = args
        # print(f"d: {dataset.shape}, w: {self.w.shape}, b: {self.b.shape}")
        # print(f"I'm predicting: w: {self.w}, b: {self.b}")

        score = mrow(self.w.T @ dataset + self.b)
        return (np.ravel(score), labels)
        
def quadratic_expansion(d: np.ndarray) -> np.ndarray:
    n = d.shape[1] # number of samples
    new_d = []
    for i in range(n):
        x = mcol(d[:, i])
        A = x @ x.T
        x_1 = mcol(A.flatten(order='F'))
        x_2 = x
        final = np.vstack([x_1, x_2])
        new_d.append(final)
    return np.hstack(new_d)   
        
class quadratic_logistic_regression(Sink):
    
    def __init__(self, l, prior):
        self.name = f'Quadratic logistic regression($\lambda = {l}$, $\pi_T = {prior}$)'
        self.trained = False
        self.l = l
        # Model parameter
        self.w = None
        self.b = None
        self.prior = prior
        
    def train(self, args):
        # Inner class useful to compute the objective
        class logRegClass:
            def __init__(self, DTR, LTR, l, prior):
                self.DTR = DTR
                self.LTR = LTR
                self.l = l
                self.prior = prior
                
            def logreg_obj(self, v):
                # Compute and return the objective function value. You can
                # retrieve all required information from self.DTR, self.LTR
                Z = self.LTR * 2.0 - 1.0
                M = self.DTR.shape[0]
                w, b = mcol(v[0:M]), v[-1]
                # Regularization term, useful to find solution with 
                # low norm
                reg_term = 0.5*self.l*np.linalg.norm(w)**2
                
                # Score computation
                # 2 classes
                d1 = self.DTR[:, self.LTR == 1]
                d2 = self.DTR[:, self.LTR == 0]
                # print(f"d1: {d1.shape}, d2: {d2.shape}")
                Z1 = Z[Z == 1]
                Z2 = Z[Z == -1]
                # print(f"z1: {Z1.shape}, Z2: {Z2.shape}")
                S1 = w.T @ d1 + b

                cxe1 = np.logaddexp(0, -S1*Z1).mean()
                
                S2 = w.T @ d2 + b
                # print(f"S1: {S1.shape}, S2: {S2.shape}")
                cxe2 = np.logaddexp(0, -S2*Z2).mean()
                # print(f'Reg_term: {reg_term}, Sum: {sum_term}')
                return reg_term + self.prior* cxe1 + (1 - self.prior)*cxe2;
            
        (dataset, label) = args
        dataset = quadratic_expansion(dataset)
        logReg = logRegClass(dataset, label, self.l, self.prior)        
        sol =  scipy.optimize.fmin_l_bfgs_b(logReg.logreg_obj, np.zeros((dataset.shape[0] + 1, 1)), approx_grad=True)
        self.w = mcol(sol[0][0:dataset.shape[0]])
        self.b = sol[0][-1]
        # print(f"I'm training the model: w: {self.w}, b: {self.b}")
        self.trained = True
    
    def run(self, args):
        (dataset, labels) = args
        dataset = quadratic_expansion(dataset)
        # print(f"d: {dataset.shape}, w: {self.w.shape}, b: {self.b.shape}")
        # print(f"I'm predicting: w: {self.w}, b: {self.b}")

        score = self.w.T @ dataset + self.b
        predicted = score > 0
        return (np.ravel(score), labels)

class SVM_linear(Sink):

    def __init__(self, K, C):
        self.name = f'Support Vector Machine Linear( C = {C}, K = {K})'
        self.trained = False
        self.C = C
        self.K = K
        # Model parameter
        self.w_hat = None

    def objectiveFunctionModifiedDualSVM(self, alpha, H):
        gradient = np.dot(H, alpha) - np.ones(H.shape[0])
        f = -np.dot(alpha.T, np.ones(H.shape[0])) + 1 / 2 * np.dot(np.dot(alpha.T, H), alpha)
        return f, gradient

    def train(self, args):
        (dataset, labels) = args
        LTR = 2 * labels - 1

        new_row = np.zeros(dataset.shape[1]) + self.K
        D_hat = np.vstack([dataset, new_row])

        G_hat = np.dot(D_hat.T, D_hat)
        zizj = np.dot(LTR.reshape(LTR.size, 1), LTR.reshape(1, LTR.size))

        H_hat = zizj * G_hat

        b = [(0, self.C)] * dataset.shape[1]
        print("Optmizing the function")
        alpha, _, _ = scipy.optimize.fmin_l_bfgs_b(self.objectiveFunctionModifiedDualSVM, np.zeros(dataset.shape[1]),
                                                    args=(H_hat,),
                                                    bounds=b, factr=10**4, iprint=1, maxfun=10**5)

        self.w_hat = np.sum((alpha * LTR) * D_hat, axis=1)

        # print(f"I'm training the model: w: {self.w}, b: {self.b}")
        self.trained = True

    def run(self, args):
        (dataset, labels) = args

        new_row = np.zeros(dataset.shape[1]) + self.K
        DTE_hat = np.vstack([dataset, new_row])

        score = np.dot(self.w_hat, DTE_hat)
        
        return (score, labels)

class SVM_polynomial(Sink):

    def __init__(self, K, C, d, c):
        self.name = f'Support Vector Machine Polynomial( C = {C}, K = {K}, d = {d}, c={c})'
        self.trained = False
        self.DTR = None
        self.LTR = None
        self.C = C
        self.K = K
        self.d = d
        self.c = c
        # Model parameter
        self.x = None

    def objectiveFunctionModifiedDualSVM(self, alpha, H):
        gradient = np.dot(H, alpha) - np.ones(H.shape[0])
        f = -np.dot(alpha.T, np.ones(H.shape[0])) + 1 / 2 * np.dot(np.dot(alpha.T, H), alpha)
        return f, gradient

    def train(self, args):
        (dataset, labels) = args
        self.DTR = dataset
        self.LTR = 2 * labels - 1

        kernelFunction = (np.dot(self.DTR.T, self.DTR) + self.c) ** self.d + self.K ** 2
        zizj = np.dot(self.LTR.reshape(self.LTR.size, 1), self.LTR.reshape(1, self.LTR.size))
        H = zizj * kernelFunction
        b = [(0, self.C)] * self.DTR.shape[1]
        (x, _, _) = scipy.optimize.fmin_l_bfgs_b(self.objectiveFunctionModifiedDualSVM, np.zeros(self.DTR.shape[1]), args=(H,),
                                           bounds=b,
                                           factr=1.0)
        self.x  = x

        # print(f"I'm training the model: w: {self.w}, b: {self.b}")
        self.trained = True

    def run(self, args):
        (dataset, labels) = args

        score = np.sum(np.dot((self.x * self.LTR).reshape(1, self.DTR.shape[1]), (np.dot(self.DTR.T, dataset) + self.c) ** self.d + self.K), axis=0)

        return (score, labels)


class SVM_RBF(Sink):

    def __init__(self, K, C, gamma):
        self.name = f'Support Vector Machine RBF( C = {C}, K = {K}, gamma={gamma})'
        self.trained = False
        self.C = C
        self.K = K
        self.gamma = gamma
        self.DTR = None
        self.LTR = None
        # Model parameter
        self.x = None

    def objectiveFunctionModifiedDualSVM(self, alpha, H):
        gradient = np.dot(H, alpha) - np.ones(H.shape[0])
        f = -np.dot(alpha.T, np.ones(H.shape[0])) + 1 / 2 * np.dot(np.dot(alpha.T, H), alpha)
        return f, gradient

    def RBF(self, x1, x2, gamma, K):
        return np.exp(-gamma * (np.linalg.norm(x1 - x2) ** 2)) + K ** 2

    def train(self, args):
        (dataset, labels) = args
        self.DTR = dataset
        self.LTR = 2 * labels - 1

        print("Compute the kernel function")
        # not so efficient
        # kernelFunction = np.zeros((self.DTR.shape[1], self.DTR.shape[1]))
        # for i in range(self.DTR.shape[1]):
        #     for j in range(self.DTR.shape[1]):
        #         kernelFunction[i, j] = self.RBF(self.DTR[:, i], self.DTR[:, j], self.gamma, self.K)
        
        # Efficient way to compute the kernel function of the RBF:
        distances = mcol((self.DTR**2).sum(0)) + mrow((self.DTR**2).sum(0)) - 2 * self.DTR.T @ self.DTR
        H = np.exp(-self.gamma*distances)
        H = mcol(self.LTR) * mrow(self.LTR) * H
        # zizj = np.dot(self.LTR.reshape(self.LTR.size, 1), self.LTR.reshape(1, self.LTR.size))
        # H = zizj * kernelFunction
        
        b = [(0, self.C)] * self.DTR.shape[1]
        print("Optmizing")
        (x, _, _) = scipy.optimize.fmin_l_bfgs_b(self.objectiveFunctionModifiedDualSVM, np.zeros(self.DTR.shape[1]), args=(H,),
                                                    bounds=b,
                                                    factr=1)
        self.x = x
        # print(f"I'm training the model: w: {self.w}, b: {self.b}")
        self.trained = True

    def run(self, args):
        (dataset, labels) = args
        kernelFunction = np.zeros((self.DTR.shape[1], dataset.shape[1]))
        for i in range(self.DTR.shape[1]):
            for j in range(dataset.shape[1]):
                kernelFunction[i, j] = self.RBF(self.DTR[:, i], dataset[:, j], self.gamma, self.K)
        score = np.sum(np.dot((self.x * self.LTR).reshape(1, self.DTR.shape[1]), kernelFunction), axis=0)

        return (score, labels)
    
def E_step(X, gmm):
    S = np.zeros((len(gmm), X.shape[1]))
    for g in range(len(gmm)):
        S[g, :] = logpdf_GAU_ND(X, gmm[g][1], gmm[g][2]) + np.log(gmm[g][0])
    marginal = scipy.special.logsumexp(S, axis=0)
    logDensities = S - marginal
    return np.exp(logDensities), marginal.sum()/X.shape[1]

def M_step(X, gmm, responsibilities):
    new_gmm = []
    for i in range(len(gmm)):
        gamma = responsibilities[i, :]
        Z = gamma.sum()
        F = (mrow(gamma)*X).sum(1)
        S = X @ (mrow(gamma)*X).T
        
        w = Z/X.shape[1]
        mu = mcol(F/Z)
        sigma = S/Z - mu @ mu.T
        # Constrain the eigenvalues
        U, s, _ = np.linalg.svd(sigma)
        s[s < psi] = psi
        sigma = U @ (mcol(s)*U.T)
        new_gmm.append((w, mu, sigma))
    return new_gmm

def fit_GMM(X, gmm):
    old_ll = None
    new_ll = None
    delta = 1e-6
    # i = 0
    while old_ll is None or new_ll - old_ll > delta:
        old_ll = new_ll
        # compute the responsibilities at E step
        responsibilities, new_ll = E_step(X, gmm)
        
        # compute the new GMM parameter at M step
        gmm = M_step(X, gmm, responsibilities)
    #     if (i != 0):
    #         print(f"#{i} {new_ll - old_ll}")
    #     i += 1
    # print(f"Average log-likelihood: {new_ll}")
    return gmm

def split_parameter(dataset, alpha, GMM_1):
    mu = GMM_1[1]
    C = GMM_1[2]
    U, s, Vh = np.linalg.svd(C)
    displacement = U[:, 0:1] * s[0]**0.5 * alpha
    return (GMM_1[0]/2, mu + displacement, C), (GMM_1[0]/2, mu - displacement, C)

def LBG_algorithm(dataset, alpha, M):
    count = 2
    mu = compute_empirical_mean(dataset)
    C = compute_empirical_cov(dataset)
    U, s, _ = np.linalg.svd(C)
    s[s < psi] = psi
    C = U @ (mcol(s)*U.T)
    current_gmm = [(1, mu, C)]
    while (count <= M):
        local_gmm = []
        # At each iteration, split each of the gmm using the split algorithm
        for gmm in current_gmm:
            gmm1, gmm2 = split_parameter(dataset, alpha, gmm)
            local_gmm.append(gmm1)
            local_gmm.append(gmm2)
            # fit the GMM using the new parameter
        # print(f"Current GMM: {count}")
        current_gmm = fit_GMM(dataset, local_gmm)
        count *= 2
    return current_gmm
    
class GMM(Sink):
    def __init__(self, components):
        self.name = f"GMM($G = {components}$)"
        self.components = components
        
        # model parameter
        self.gmm = []
        
    def train(self, args):
        (dataset, labels) = args
        gmm_model = []
        # each row contains a gmm model, for the given label
        for label in range(2):
            d = dataset[:, labels == label]
            l = labels[labels == label]
            gmm_model.append(LBG_algorithm(d, .1, self.components))
        self.gmm = gmm_model
    
    def run(self, args):
        (dataset, labels) = args
        logS = np.zeros((2, dataset.shape[1]));
        for label in range(2):
            gmm = self.gmm[label]
            logS[label, :] = mrow(logpdf_GMM(dataset, gmm))  + np.log(1/2)
        logSMarginal = mrow(scipy.special.logsumexp(logS, axis=0))
        logSPost = logS - logSMarginal
        llr = logSPost[1, :] - logSPost[0, :]
        return (np.ravel(llr), labels)