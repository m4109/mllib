"""_summary_

Utilities to compute probability density function
or likehood.

author: gabbiurlaro
"""
import numpy as np
from mllib.utils import mcol, mrow
import scipy.special

def logpdf_1Sample(x: np.ndarray, mu: np.ndarray, C:np.ndarray) -> np.ndarray:
    """Compute the log probability density function.
    
    #### Parameters: 
     - x: the column vector represeting the data
     - mu: column vector representing the mean
     - C: Covariance matrix
    """
    # costant term
    y = - x.shape[0]/2*np.log(2*np.pi)
    # term usign the determinant
    y += -np.linalg.slogdet(C)[1]/2
    #final term
    y += -1/2*np.dot(np.dot((x - mu).T, np.linalg.inv(C)), (x - mu))
    return y.ravel()


def logpdf_GAU_ND(X: np.ndarray, mu: np.ndarray, C: np.ndarray) -> np.ndarray:
    """Compute the log probability density function.
    
    #### Parameters: 
     - X: dataset(in column form) represeting the data
     - mu: column vector representing the mean
     - C: Covariance matrix
    """
    # optimized version
    P = np.linalg.inv(C)
    return -0.5 * X.shape[0] * np.log(np.pi * 2) + 0.5 * np.linalg.slogdet(P)[1] - 0.5 * (np.dot(P, (X - mu)) * (X - mu)).sum(0)

    
    # result = []
    # for i in range(X.shape[1]):
    #     # x is the single sample. for each sample
    #     # compute the log Normal distribution
    #     x = X[:, i]
    #     # print(x)
    #     # print(x.y.ravel(shape[0])
    #     y = logpdf_1Sample(mcol(x), mu, C)
    #     result.append(y)
    #     #print(y)
    # return np.array(result).ravel()


def compute_empirical_mean(X: np.ndarray) -> np.ndarray:
    """Compute the empirical mean for a dataset. 
    
    It returns a column vector, each element corresponds to the mean of the given row(representing an
    attribute).

    #### Parameters:
     - X: dataset in column form(each column represent a sample)
    """
    return mcol(X.mean(1))

def compute_wc_covariance_matrix(d, l):
   Sw = 0;
   for i in [0, 1]:
       Sw += (l==i).sum()*compute_empirical_cov(d[:, l == i])
   return Sw/d.shape[1]


def compute_empirical_cov(X: np.ndarray) -> np.ndarray:
    """
    Compute the empirical covariance matrix for a dataset. 

    #### Parameters:
     - X: dataset in column form(each column represent a sample)
    """
    mu =  compute_empirical_mean(X)
    cov = np.dot((X - mu), (X - mu).T) / X.shape[1]
    return cov

def z_normalization(d: np.ndarray) -> np.ndarray:
    mu = mcol(compute_empirical_mean(d))
    standard_dev = mcol(d.std(axis=1)) # should be faster
    # standard_dev = mcol(np.sqrt(
    #                 1/(d.shape[1] - 1)*((d - mu)*(d - mu)).sum(axis=1)
    #                 ))
    # print("standard dev ", standard_dev.shape)
    return mu, standard_dev

def loglikelihood(x: np.ndarray, mu: np.ndarray, C: np.ndarray) -> np.ndarray:
    """Compute the log likehood

    #### Parameters: 
     - X: dataset(in column form) represeting the data
     - mu: column vector representing the mean
     - C: Covariance matrix
    """
    return logpdf_GAU_ND(x, mu, C).sum(0)

def pdf_gaussian(X: np.ndarray, mu: np.ndarray, C: np.ndarray) -> np.ndarray:
    """Compute the probability density function
    #### Parameters: 
     - X: dataset(in column form) represeting the data
     - mu: column vector representing the mean
     - C: Covariance matrix
    """
    return np.exp(logpdf_GAU_ND(X, mu, C))

def logpdf_GMM(X: np.ndarray, gmm) -> np.ndarray:
    """Compute the probability density function of a GMM given the GMM components
    #### Parameters: 
     - X: dataset(in column form) represeting the data
     - gmm an array of tuple (w, mu, C)
    """
    S = np.zeros((len(gmm), X.shape[1]))
    for g in range(len(gmm)):
        S[g, :] = logpdf_GAU_ND(X, gmm[g][1], gmm[g][2]) + np.log(gmm[g][0])
    logdens = scipy.special.logsumexp(S, axis=0)
    return logdens
