# -*- coding: utf-8 -*-
"""
Created on Fri Apr  1 11:39:19 2022

@author: gabbi
"""
import numpy as np
import matplotlib.pyplot as plt


def mcol(a: np.ndarray) -> np.ndarray:
    """Given a numpy vector returns the correspective column vector
    
    ##### Parameter:
    - a: np array
    """
    return a.reshape(a.size, 1)

def mrow(a: np.ndarray) -> np.ndarray:
    """Given a numpy vector returns the correspective row vector
    ##### Parameter:
    - a: np array
    """
    return a.reshape(1, a.size)

def split_db_2to1(D, L, seed=0):
    """
    Parameters
    ----------
    D : np.ndarray 
        Dataset in column form(each column represent a sample).
    L : np.ndarray
        array of labels.
    seed : int, optional
        seed for the permutation. The default is 0.

    Returns
    -------
    ((np.ndarray, np.ndarray),(np.ndarray, np.ndarray))
        The two partition of the dataset and the label.
    """
    nTrain = int(D.shape[1]*2.0/3.0)
    np.random.seed(seed)
    idx = np.random.permutation(D.shape[1])
    idxTrain = idx[0:nTrain]
    idxTest = idx[nTrain:]
    DTR = D[:, idxTrain]
    DTE = D[:, idxTest]
    LTR = L[idxTrain]
    LTE = L[idxTest]
    return (DTR, LTR), (DTE, LTE)

def shuffle(D, L, seed=0):
    np.random.seed(seed) 
    idx = np.random.permutation(D.shape[1])
    return D[:, idx], L[idx]