"""step.py
Define the abstract classes that represent the pieces of the pipeline.
"""

class Source():
    def __init__(self):
        self.name = "Source"
    
    def __str__(self):
        return f'{self.name} '
    
class Pipe():
    def __init__(self):
        self.name = "Pipe"
    
    def __str__(self):
        return f'{self.name} '
    
class Sink():
    def __init__(self):
        self.name = "Sink"
    
    def __str__(self):
        return self.name
    