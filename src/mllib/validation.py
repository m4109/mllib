# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 18:39:23 2022

@author: gabbi
"""
import numpy as np
from tabulate import tabulate
import matplotlib.pyplot as plt

from mllib.Classifier import linear_logistic_regression
from mllib.utils import *
from mllib.pipeline import Pipeline
import time

# UTILS
def bayes_plot_DCF(score, labels, title):
    effPriorLogOdds = np.linspace(-3, 3, 50)
    dcf = []
    min_dcf = []
    for p in effPriorLogOdds:
        pr = 1/(1 + np.exp(-p))
        working_point = (pr, 1, 1)
        predicted = binary_optimal_bayes_decision(working_point, score)
        cf = confusion_matrix(predicted, labels, 2)
        min_dcf.append(minimum_detectiong_cost(working_point, score, labels))
        dcf.append(normalized_binary_DCF(working_point, cf))
    
    plt.plot(effPriorLogOdds, dcf, label='actual DCF', color='brown', linestyle="solid")
    plt.plot(effPriorLogOdds, min_dcf, label='min DCF', color='r', linestyle='dashed')
    # plt.title(title)
    plt.ylim([0, 1])
    plt.xlim([-3, 3])
    plt.grid(True)
    plt.legend()

    # plt.savefig("img/bayes/actDCF" + title)
    
def minimum_detectiong_cost(working_point, llr, labels):
    #(p, fn, fp) = working_point
    scores = np.sort(llr)
    # print(scores.shape)
    dcfs = []
    for i in range(scores.size):
        t = scores[i]
        pred = binary_optimal_bayes_decision(working_point, llr, t)
        cf = confusion_matrix(pred, labels, 2)
        dcfs.append(normalized_binary_DCF(working_point, cf))
    return min(dcfs)

def actual_DCF(scores, labels, working_point):
    pred = binary_optimal_bayes_decision(working_point, scores)
    cf = confusion_matrix(pred, labels, 2)
    return normalized_binary_DCF(working_point, cf)
    
def binary_optimal_bayes_decision(params, llr, t=None):
    (p, fn, fp) = params
    # print(t)
    if (t == None):
        t = - np.log((p * fn)/((1-p)*fp))
    return llr > t

def confusion_matrix(predicted, expected, labels):
    k = labels
    cm = np.zeros((k,k))
    for i in range(k):
        for j in range(k):
            cm[i, j] = ((predicted == i) * (expected == j)).sum()
    return cm

def normalized_binary_DCF(working_point, cf):
    (p, cfn, cfp) = working_point
    return (binary_DCF(working_point, cf))/min(p*cfn, (1-p)*cfp)

def binary_DCF(working_point, cf):
    (p, cfn, cfp) = working_point
    FNR, FPR = compute_fnr_fpr(cf)
    return p*cfn*FNR + (1-p)*cfp*FPR

def compute_fnr_fpr(cf):
    return (cf[0, 1]/(cf[0,1] + cf[1, 1]), cf[1, 0]/(cf[1, 0] + cf[0, 0]))

def split_and_shuffle(dataset, labels, ratio=0.6):
    n = int(dataset.shape[0]*ratio)
    idx = np.random.permutation(dataset.shape[0])
    print(idx)
    idxTrain = idx[0:n]
    idxTest = idx[n:]
    DTR = dataset[idxTrain]
    DTE = dataset[idxTest]
    LTR = labels[idxTrain]
    LTE = labels[idxTest]
    return (mrow(DTR), LTR), (DTE, LTE)

class K_fold_cross_validation():
    def __init__(self, k, pipelines):
        self.k = k
        self.pipelines = pipelines
        self.scores = []
        self.labels = []
        
        self.fused_scores = []
        self.calibrated_scores = []
        
    def validate(self, dataset, label, save=False):
        foldSize = int(dataset.shape[1]/self.k)
        for pipeline in self.pipelines:
            print(f"Training {pipeline.__str__()}")
            start_total = time.time()
            performance = []
            label_ordered = []
            for i in range(self.k):
                start = time.time()
                train_set = np.hstack([dataset[:, 0:i*foldSize], dataset[:, (i+1)*foldSize:dataset.shape[1]]])
                train_label = np.hstack([label[0:i*foldSize], label[(i+1)*foldSize:label.shape[0]]])
                
                validation_set = dataset[:, i*foldSize:(i+1)*foldSize]
                validation_label = label[i*foldSize:(i+1)*foldSize]
                
                pipeline.train((train_set, train_label))
                (score, actual) = pipeline.run((validation_set, validation_label))
                # predicted = score > 0
                performance.append(score)
                label_ordered.append(actual)
                print(f"\t #{i} fold {time.time() - start}")
            
            performance = np.hstack(performance)
            label_ordered = np.hstack(label_ordered)
            # print(f"Performance: {performance.shape}")
            # print(f"Labels: {label_ordered.shape}")
            # To be safe, we save the scores and labels
            if(save):
                np.save(f'scores/{pipeline.__str__()}_scores.npy', performance)
                np.save(f'scores/{pipeline.__str__()}_labels.npy', label_ordered)
            self.scores.append(performance)
            self.labels.append(label_ordered)
            print(f"--- {time.time() - start_total} s ---")
    
    def accuracy(self):
        i = 0
        priors = [.5, .9, .1]
        data = []
        for pip in self.pipelines:
            acc = []
            for j in [0, 1, 2]:
                pred = binary_optimal_bayes_decision((priors[j], 1, 1), self.scores[i])
                acc.append((pred == self.labels[i]).sum()/len(self.labels[i]))
            row = [pip.__str__(), acc[0], acc[1], acc[2]]
            data.append(row)
            
        print(tabulate(data, headers=["Accuracy prior = 0.5", "Accuracy prior = 0.9", "Accuracy prior = 0.1"]))
    
    def fusion(self):
        """
        compute the minDCF of the fused model(with teh first two)

        """
        # if ( len(self.pipelines) != 2):
        #     return False
        initial_scores = np.vstack(self.scores)
        label = self.labels[0]
        lr = linear_logistic_regression(1e-3, .5)
        (DTR, LTR), (DTE, LTE) = split_db_2to1(initial_scores, label)
        lr.train((DTR, LTR))
        fused_scores, lab =  lr.run((DTE, LTE))
        return minimum_detectiong_cost((.5, 1, 1), fused_scores, lab)
    
    def print_result(self, actual_dcf=False, latex=False):
        data = []
        priors = [.5 ,.9, .1]
        min_dcf = []
        act_dcf = []
        i = 0
        for pip in self.pipelines:
            for j in [0, 1, 2]:
                min_dcf.append(minimum_detectiong_cost((priors[j], 1, 1), self.scores[i], self.labels[i]))
                if(actual_dcf):   
                    act_dcf.append(actual_DCF(self.scores[i], self.labels[i], (priors[j], 1, 1)))
            if(actual_dcf):
                row = [pip.__str__(), min_dcf[0], act_dcf[0], min_dcf[1], act_dcf[1], min_dcf[2], act_dcf[2]]
            else:
                row = [pip.__str__(), min_dcf[0], min_dcf[1], min_dcf[2]]
            min_dcf.clear()
            act_dcf.clear()
            data.append(row)
            i += 1
        if(actual_dcf):
            print(tabulate(data, headers=["Model", "minDCF prior = 0.5", "actual DCF prior = 0.5",
                                               "minDCF prior = 0.9", "actual DCF prior = 0.9", 
                                               "minDCF prior = 0.1", "actual DCF prior = 0.1"  ]))
        else:
            print(tabulate(data, headers=["Model", "minDCF prior = 0.5",
                                               "minDCF prior = 0.9", 
                                               "minDCF prior = 0.1"]))
        if(latex):
            if(actual_dcf):
                print("Latex format\n\n")
                print("\\begin{table}[H]")
                print("\\centering")
                print("\\begin{tabular}{c|c c|c c|c c }")
                print("Model & $DCF_{min}$ prior = 0.5 & $DCF_{act}$ prior = 0.5 &", 
                      "$DCF_{min}$ prior = 0.9 & $DCF_{min}$  prior = 0.9  &", 
                      "$DCF_{min}$ prior = 0.1 & $DCF_{act}$  prior = 0.1 \\\\")
                print("\\hline")
                for row in data:
                    print(f"{row[0]} & {row[1]:.4f} & {row[2]:.4f} & {row[3]:.4f} & {row[4]:.4f} & {row[5]:.4f} & {row[6]:.4f}\\\\")
            else:
                print("Latex format\n\n")
                print("\\begin{table}[H]")
                print("\\centering")
                print("\\begin{tabular}{c|c c c}")
                print("Model & prior = 0.5 & prior = 0.9 & prior = 0.1 \\\\")
                print("\\hline")
                for row in data:
                    print(f"{row[0]} & {row[1]:.4f} & {row[2]:.4f} & {row[3]:.4f} \\\\")
            print("\\end{tabular}")       
            print("\\end{table}")
    
    def BayesErrorPlot(self, calibrated=False):
        for pip, scores, labels in zip(self.pipelines, self.scores, self.labels):
            plt.figure()
            bayes_plot_DCF(scores, labels,"Un-calibrated scores " + pip.__str__() )
            plt.show()
            if(calibrated == True):
                # calibrate the scores using the LR model
                (DTR, LTR), (DTE, LTE) = split_and_shuffle(scores, labels)
                # Estimate on the DTR, LTR the parameter of the calibration function
                prior = .5
                lr = linear_logistic_regression(0.01, prior)
                lr.train((DTR, LTR))
                # print(f"w: {lr.w}, b: {lr.b}")
                # Calibration
                calibrated_scores = np.ravel(lr.w*DTE + lr.b - np.log((prior/(1-prior))))
                # print("dopo", self.scores[i].shape)
                plt.figure()
                bayes_plot_DCF(calibrated_scores, LTE, "Calibrated scores " + pip.__str__() )
                plt.show()
                
    def mindcf(self, prior):
        min_dcf = []
        for pip, scores, labels in zip(self.pipelines, self.scores, self.labels):
            min_dcf.append(minimum_detectiong_cost((prior, 1, 1), scores, labels))
        return min_dcf
