# -*- coding: utf-8 -*-
"""
Created on Fri Jun 17 10:12:48 2022

@author: gabbi
"""

from mllib.pipeline import Pipeline
from mllib.Classifier import mvg_classifier, naive_bayes_classifier, tied_gaussian_classifier, linear_logistic_regression, quadratic_logistic_regression, GMM 
from mllib.load_data import Data
from mllib.validation import K_fold_cross_validation
from mllib.preprocessing import PCA, Z_Norm
from mllib.utils import shuffle
import numpy as np
import time

def split_db_2to1(D, L, seed=0):
    """
    Parameters
    ----------
    D : np.ndarray 
        Dataset in column form(each column represent a sample).
    L : np.ndarray
        array of labels.
    seed : int, optional
        seed for the permutation. The default is 0.

    Returns
    -------
    ((np.ndarray, np.ndarray),(np.ndarray, np.ndarray))
        The two partition of the dataset and the label.
    """
    nTrain = int(D.shape[1]*2.0/3.0)
    np.random.seed(seed)
    idx = np.random.permutation(D.shape[1])
    idxTrain = idx[0:nTrain]
    idxTest = idx[nTrain:]
    DTR = D[:, idxTrain]
    DTE = D[:, idxTest]
    LTR = L[idxTrain]
    LTE = L[idxTest]
    return (DTR, LTR), (DTE, LTE)

if __name__ == "__main__":
    components = [2**(n+1) for n in range(5)]
    p = []
    for g in components:
        p.append(Pipeline([Z_Norm(), GMM(g)]))
        
    (dataset, labels) = Data("IRIS_BINARY").run()
    ((DTR, LTR), (DTE, LTE)) = split_db_2to1(dataset, labels)
    for model in p:
        model.train((DTR, LTR))
        (score, _) = model.run((DTE, LTE))
        prediction = score > 0
        acc = np.array([prediction == LTE]).sum()/LTE.shape[0]
        print(f"{model} \t Error rate: {(1 - acc)*100:6.3f} %")