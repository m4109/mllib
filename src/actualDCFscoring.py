# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 17:12:58 2022

@author: gabbi
"""
from mllib.pipeline import Pipeline
from mllib.Classifier import *
from mllib.load_data import Data
from mllib.validation import K_fold_cross_validation
from mllib.preprocessing import PCA, Z_Norm
from mllib.utils import shuffle
import numpy as np
from mllib.utils import split_db_2to1, shuffle
import time

if __name__ == "__main__":
    p = [
        # Pipeline([Z_Norm(), tied_gaussian_classifier(.5)]),
        # Pipeline([Z_Norm(), linear_logistic_regression(1e-4, .5)]),
        # Pipeline([Z_Norm(), GMM(16)]),
        # Pipeline([Z_Norm(), PCA(8, 7), quadratic_logistic_regression(1e-4, .1)]),
        # Pipeline([Z_Norm(), SVM_linear(1, 5e-2)]),
    ]
    
    (dataset, label) = Data("PULSAR").run()
    (dataset, label) = shuffle(dataset, label, 140)
    (DTR, LTR), (DTE, LTE) = split_db_2to1(dataset, label)

    k = K_fold_cross_validation(3, p)
    start = time.time();
    k.validate(dataset, label)
    print(f"Training finished in {time.time() - start} s")    
    k.print_result(actual_dcf=True, latex=True)
    # k.accuracy()
    print(f"min DCF computed in {time.time() - start} s")
    start = time.time();
    k.BayesErrorPlot(calibrated=True)
    print(f"Bayes error plot in {time.time() - start} s")
    # print("\n\nEvaluation set:\n\n")
    
    # for model in p:
    #     model.train((DTR, LTR))
    #     (score, _) = model.run((DTE, LTE))
    
    #     prediction = score > 0
    #     acc = np.array([prediction == LTE]).sum()/LTE.shape[0]
    #     print(f"{model} \t Error rate: {(1 - acc)*100:6.3f} %")