from mllib.pipeline import Pipeline
from mllib.Classifier import linear_logistic_regression, quadratic_logistic_regression
from mllib.load_data import Data
from mllib.validation import K_fold_cross_validation
from mllib.preprocessing import PCA, Z_Norm
from mllib.utils import shuffle
import numpy as np
import time
import itertools

if __name__ == "__main__":
    lambdas = [1e-5, 1e-4, 1e-3, 1e-2, 1e-1]
    priors = [.5, .9, .1]
    p = []

    for l, prior in itertools.product(lambdas, priors):
        p.append(Pipeline([Z_Norm(), PCA(8,7), quadratic_logistic_regression(l, prior)]))
    # for l, prior in itertools.product(lambdas, priors):
    #     p.append(Pipeline([Z_Norm(), PCA(8, 7), linear_logistic_regression(l, prior)]))
    # for l, prior in itertools.product(lambdas, priors):
    #     p.append(Pipeline([Z_Norm(), PCA(8, 6), linear_logistic_regression(l, prior)]))
    # for l, prior in itertools.product(lambdas, priors):
    #     p.append(Pipeline([Z_Norm(), PCA(8, 5), linear_logistic_regression(l, prior)]))   
        
    (dataset, label) = Data("PULSAR").run()
    # For reproducibility, we shuffle the dataset with a given seed
    (dataset, label) = shuffle(dataset, label, 140)
    
    # Define a K-fold cross validation object with k = 3
    k = K_fold_cross_validation(3, p)
    start = time.time();
    k.validate(dataset, label)
    print(f"Training finished in {time.time() - start} s")    
    start = time.time()
    k.print_result(latex=True)
    # k.accuracy()
    print(f"MIN DCF computation in {time.time() - start} s")
    # start = time.time();
    # k.BayesErrorPlot(calibrated=True)
    # print(f"Bayes error plot in {time.time() - start} s")
    # print("\n\nEvaluation set:\n\n")
    
    # for model in p:
    #     model.train((DTR, LTR))
    #     (score, _) = model.run((DTE, LTE))
    
    #     prediction = score > 0
    #     acc = np.array([prediction == LTE]).sum()/LTE.shape[0]
    #     print(f"{model} \t Error rate: {(1 - acc)*100:6.3f} %")