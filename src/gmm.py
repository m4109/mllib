# -*- coding: utf-8 -*-
"""
Created on Sat Jun 18 19:32:43 2022

@author: gabbi
"""

from mllib.pipeline import Pipeline
from mllib.Classifier import GMM, mvg_classifier
from mllib.load_data import Data
from mllib.validation import K_fold_cross_validation
from mllib.preprocessing import PCA, Z_Norm
from mllib.utils import shuffle
import numpy as np
import time

import matplotlib.pyplot as plt
if __name__ == "__main__":
    components = [8, 16]
    p = []

    for g in components:
        p.append(Pipeline([Z_Norm(), GMM(g)]))
                
    # p.append(Pipeline([Z_Norm(), mvg_classifier(.5)]))
    (dataset, label) = Data("PULSAR").run()
    # For reproducibility, we shuffle the dataset with a given seed
    (dataset, label) = shuffle(dataset, label, 140)
    
    # Define a K-fold cross validation object with k = 3
    k = K_fold_cross_validation(3, p)
    start = time.time();
    k.validate(dataset, label)
    print(f"Training finished in {time.time() - start} s")    
    start = time.time()
    k.print_result(latex=True)
    k.accuracy()
    k.BayesErrorPlot()
    # print(f"MIN DCF computation in {time.time() - start} s")
    # start = time.time();
    # k.BayesErrorPlot(calibrated=True)
    # print(f"Bayes error plot in {time.time() - start} s")
    # print("\n\nEvaluation set:\n\n")
    
    # for model in p:
    #     model.train((DTR, LTR))
    #     (score, _) = model.run((DTE, LTE))
    
    #     prediction = score > 0
    #     acc = np.array([prediction == LTE]).sum()/LTE.shape[0]
    #     print(f"{model} \t Error rate: {(1 - acc)*100:6.3f} %")