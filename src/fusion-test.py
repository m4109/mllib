# -*- coding: utf-8 -*-
"""
Created on Sun Jun 19 17:40:48 2022

@author: gabbi
"""
from mllib.pipeline import Pipeline
from mllib.Classifier import tied_gaussian_classifier, linear_logistic_regression, GMM
from mllib.load_data import Data
from mllib.validation import K_fold_cross_validation
from mllib.preprocessing import PCA, Z_Norm
from mllib.utils import shuffle
import numpy as np
import time
import itertools

if __name__ == "__main__":
    p = [
        Pipeline([tied_gaussian_classifier(.5)]), 
        Pipeline([PCA(8, 7), linear_logistic_regression(1e-4, .5)]),
        # Pipeline([GMM(16)])
    ]
      
    (dataset, label) = Data("PULSAR").run()
    # For reproducibility, we shuffle the dataset with a given seed
    (dataset, label) = shuffle(dataset, label, 140)
    
    # Define a K-fold cross validation object with k = 3
    k = K_fold_cross_validation(3, p)
    start = time.time();
    k.validate(dataset, label)
    print(f"Training finished in {time.time() - start} s")    
    start = time.time()
    k.print_result()
    fused_mindcf = k.fusion()
    print(f"Min dcf of fused model: {fused_mindcf}")
    # k.accuracy()
    print(f"MIN DCF computation in {time.time() - start} s")
    # start = time.time();
    # k.BayesErrorPlot(calibrated=True)
    # print(f"Bayes error plot in {time.time() - start} s")
    # print("\n\nEvaluation set:\n\n")
    
    # for model in p:
    #     model.train((DTR, LTR))
    #     (score, _) = model.run((DTE, LTE))
    
    #     prediction = score > 0
    #     acc = np.array([prediction == LTE]).sum()/LTE.shape[0]
    #     print(f"{model} \t Error rate: {(1 - acc)*100:6.3f} %")