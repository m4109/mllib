# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 15:52:30 2022

@author: gabbi
"""

import numpy as np
import time
import matplotlib.pyplot as plt
import itertools

if __name__ == "__main__":
    parameter = np.logspace(-1, -4, 10)
    # parameter = [0.05]
    # c = [0, 1, 10, 20]
    gamma = [10**-2, 10**-3, 10**-4]

    model_parameter = [(c, g) for c, g in itertools.product(parameter, gamma)]
    # print(parameter)
    min1 = np.load("scores/min_dcf_computation.npy")
    mindcfGamma1 = []
    mindcfGamma2 = []
    mindcfGamma3 = []

    for val, p in zip(min1, model_parameter):
        if(p[1] == 1e-2):
            mindcfGamma1.append(val)
        elif(p[1] == 1e-3):
            mindcfGamma2.append(val)
        elif(p[1] == 1e-4):
            mindcfGamma3.append(val)
    
            
    plt.figure()
    plt.plot(parameter, mindcfGamma1, color="darkorange", label="$\gamma = 10^{-2}$")
    plt.plot(parameter, mindcfGamma2, color="gold", label="$\gamma = 10^{-3}$")
    plt.plot(parameter, mindcfGamma3, color="firebrick", label="$\gamma = 10^{-4}$")
    plt.xlim([min(parameter), max(parameter)])
    plt.xscale("log", base=10)
    plt.legend()
    plt.xlabel("C")
    plt.savefig("img/bayes/rbf")