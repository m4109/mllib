# -*- coding: utf-8 -*-
"""
This file contains some 
"""
from mllib.pipeline import Pipeline
from mllib.Classifier import GMM
from mllib.load_data import Data
from mllib.validation import K_fold_cross_validation
from mllib.preprocessing import PCA, Z_Norm
from mllib.utils import shuffle

import numpy as np
import time
import matplotlib.pyplot as plt

if __name__ == "__main__":
    components = np.array([2**(n) for n in range(8)])
    p1 = []
    p2 = []    
    for g in components:
        p1.append(Pipeline([Z_Norm(), GMM(g)]))
     
    for g in components:
        p2.append(Pipeline([Z_Norm(), PCA(8, 7), GMM(g)]))
         
    (dataset, label) = Data("PULSAR").run()
    (dataset, label) = shuffle(dataset, label, 140)
    start = time.time();
    print("Training all the models with 3-fold.")

    k1 = K_fold_cross_validation(3, p1)
    k2 = K_fold_cross_validation(3, p2)

    k1.validate(dataset, label)
    k2.validate(dataset, label)

    print(f"Training finished in {time.time() - start} s")
    
    # I need to plot for different values of lamba ad different prior, the minDCF
    # on the x-axis I have lambda, on y the min_DCF, and different color for the different prior
    start = time.time()
    min1 = k1.mindcf(.5)
    min2 = k2.mindcf(.5)
    print(f"minDCF computed in {time.time() - start} s")
    
    plt.figure()
    plt.bar(components, min1, width=.2*components, color="r", label="Raw Data", align='center')
    plt.bar(components, min2, width=.2*components, color="b", label="PCA", align='edge')
    plt.xlim([min(components), max(components)])
    plt.xscale("log", base=2)
    plt.legend()
    plt.xlabel("G")
    plt.savefig("img/bayes/GMM")
    
    
    
    # fig, ax = plt.subplots()
    # width = .35 *components
    # rects1 = ax.bar(components - width/2, min1, width, label='Raw data')
    # rects2 = ax.bar(components + width/2, min1, .2*width, label='PCA')
    
    # # Add some text for labels, title and custom x-axis tick labels, etc.
    # ax.legend()
    
    # ax.bar_label(rects1, padding=3)
    # ax.bar_label(rects2, padding=3)
    # plt.xscale('log', base=2)
    # fig.tight_layout()
    
    # plt.show()
        
    