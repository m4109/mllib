"""
This file contains some
"""
from mllib.pipeline import Pipeline
from mllib.Classifier import SVM_polynomial, SVM_RBF
from mllib.load_data import Data
from mllib.validation import K_fold_cross_validation
from mllib.preprocessing import PCA, Z_Norm
from mllib.utils import shuffle

import numpy as np
import time
import matplotlib.pyplot as plt
import itertools

if __name__ == "__main__":
    parameter = np.logspace(-3, -5, 10)
    c = [20, 30, 50]
    # gamma = [10**-2, 10**-3, 10**-4]
    p1 = []
    p2 = []
    p3 = []

    for C in parameter:
        p1.append(Pipeline([Z_Norm(), SVM_polynomial(1, C, 2, c[0])]))
        p2.append(Pipeline([Z_Norm(), SVM_polynomial(1, C, 2, c[1])]))
        p3.append(Pipeline([Z_Norm(), SVM_polynomial(1, C, 2, c[2])]))

    (dataset, label) = Data("PULSAR").run()
    (dataset, label) = shuffle(dataset, label, 140)
    
    k1 = K_fold_cross_validation(3, p1)
    k2 = K_fold_cross_validation(3, p2)
    k3 = K_fold_cross_validation(3, p3)
    start = time.time()
    print("PIPELINE #1\n\n")
    k1.validate(dataset, label)
    min1 = k1.mindcf(.5)
    np.save(f"qsvm_mindcfc_{c[0]}", min1)
    print("PIPELINE #2\n\n")
    k2.validate(dataset, label)
    min2 = k2.mindcf(.5)
    np.save(f"qsvm_mindcfc_{c[1]}", min2)
    print("PIPELINE #2\n\n")

    k3.validate(dataset, label)
    min3 = k3.mindcf(.5)
    np.save(f"qsvm_mindcfc_{c[2]}", min3)
    
    print(f"Training finished in {time.time() - start}")
    # I need to plot for different values of lamba ad different prior, the minDCF
    # on the x-axis I have lambda, on y the min_DCF, and different color for the different prior

    plt.figure()
    plt.plot(parameter, min1, color="darkorange", label="c=20")
    plt.plot(parameter, min2, color="gold", label="c=30")
    plt.plot(parameter, min3, color="firebrick", label="c=50")
    plt.xlim([min(parameter), max(parameter)])
    plt.xscale("log", base=10)
    plt.legend()
    plt.xlabel("C")
    # plt.show()
    plt.savefig("img/bayes/quadratic_SVM_c_and_C")